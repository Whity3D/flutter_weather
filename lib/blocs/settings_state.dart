import 'package:equatable/equatable.dart';

enum TemperatureUnits { celsius, fahrenheit }

class SettingsState extends Equatable {
  final TemperatureUnits temperatureUnits;

  const SettingsState({required this.temperatureUnits});

  @override
  List<Object?> get props => [temperatureUnits];
}
